// Определяем зависимости в переменных
var gulp = require('gulp'),
    cache = require('gulp-cache'),
    clean = require('gulp-clean'),
    stream = require('event-stream'),
    size = require('gulp-size'),
    jshint = require('gulp-jshint'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    minifyCSS = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    imagemin = require('gulp-imagemin');

// Проверка ошибок в скриптах
gulp.task('lint', function() {
    return gulp.src(['main/js/*.js'])
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});


// Конкатенация и минификация стилей
// При указании исходников в gulp.src учитывается порядок в котором они указаны,
// то есть первыми в итоговом файле будут стили бустрапа, потому что мы должны
// вначале объявить их, чтобы потому переопределить на свои стили
// То же самое касается скриптов - мы вначале объявляем зависимости и уже потом
// подключаем наши скрипты (например первым будет всегда jquery, если он используется
// в проекте, а уже следом все остальные скрипты)
gulp.task('styles', function() {
    return gulp.src(
            [
                'vendor/twbs/bootstrap/dist/css/*.css',
                '!vendor/twbs/bootstrap/dist/css/*.css.map',
                '!vendor/twbs/bootstrap/dist/css/*.min.css',
                'web/main/css/*.css'
            ]
        )
        .pipe(concat('all.css'))
        .pipe(minifyCSS({
            keepBreaks: true
        }))
        .pipe(gulp.dest('web/assets/css'));
});

// Конкатенация и минификация скриптов
// Тут выделяются два контекста - jquery-плагины / наши скрипты и зависимости (то без чего
// не будет работать хотя бы один наш скрипт, например jquery)
// Так как это просто пример, то лучшим вариантом было бы разделение на основные и
// вспомогательные скрипты (например основные - jquery/bootstrap и вспомогательные - lightbox/fotorama)
gulp.task('scripts', function() {
    var js = gulp.src(
            [
                'vendor/components/jquery/jquery.js',
                'vendor/twbs/bootstrap/dist/js/*.js',
                '!vendor/twbs/bootstrap/dist/js/*.min.js'
            ]
        )
        .pipe(concat('all.js'))
        .pipe(uglify())
        .pipe(size({
            title: 'size of custom js'
        }))
        .pipe(gulp.dest('web/assets/js'));
    stream.concat(js);
});

// Чистим директорию назначения и делаем ребилд, чтобы удаленные из проекта файлы не остались
gulp.task('clean', function() {
    return gulp.src(['web/assets/css', 'web/assets/js'], {read: false})
        .pipe(clean());
});

// Наблюдение за изменениями и автосборка
// После первого запуска (команда gulp в консоли) выполняем gulp watch,
// чтобы следитть за изменениями и автоматически пересобирать исходники с учетом
// последних изменений
gulp.task('watch', function() {
    gulp.watch('web/main/js/*.js', ['lint', 'scripts']);
    gulp.watch('web/main/css/*.css', ['styles']);
});

// Выполняем по-умолчанию (вначале очистка и ребилд директории назначения, а потом выполнение остальных задач)
gulp.task('default', ['clean'], function() {
    gulp.start('styles', 'scripts');
});