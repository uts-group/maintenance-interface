<?php
require_once __DIR__.'/../vendor/autoload.php';
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Dominator {

    private static $instance = NULL;
    public $request;
    public $response;
    private $basePath = '/var/www/hbconnect/';

    public function __construct()
    {
        $this->request = Request::createFromGlobals();
        $this->response = new Response();
    }

    public static function getInstance()
    {
        if (self::$instance == NULL) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function run()
    {
        if ($action = $this->request->get('action')) {
            //$this->response->headers->set('Content-Type', 'application/json');
            $method = $action . 'Action';
            $id = $this->request->request->get('id');
            $value = $this->request->request->get('value');
            if (method_exists($this, $method) && ($response = $this->$method($id, $value))) {
                //$this->response->setContent(json_encode($response));
                header('Location: '. $this->request->getRequestUri());
                exit;
            }
            else {
                $this->response->setStatusCode(403);
            }
        }
        else {
            $domains = $this->getDomainsStatus( $this->getDomains() );
            ob_start();
            include_once 'views/base.phtml';
            $content = ob_get_clean();
            $this->response->setContent($content);
        }
        $this->response->send();
    }

    /**
     * @return array
     */
    public function getDomains()
    {
        // Временно так, потом считывания поддоменов должно вытягиваться из nginx
        $domainsForTestOnly = [
            'hbconnect.ru' => 'webroot',
            'api.hbconnect.ru' => 'yii/apps/hbconnect/api/web',
            'dev.hbconnect.ru' => 'yii/apps/hbconnect/backend/web',
            'new.hbconnect.ru' => 'yii/apps/hbconnect/frontend/web',
        ];
        return $domainsForTestOnly;
    }

    public function getDomainsStatus($domainsIn)
    {
        $domainsOut = [];
        foreach ($domainsIn as $domainName => $domainPath) {
            if (file_exists($this->basePath.$domainPath.'/maintenance.file')) {
                $domainsOut[ $domainName ] = FALSE;
            }
            else {
                $domainsOut[ $domainName ] = TRUE;
            }
        }
        return $domainsOut;
    }

    /**
     * @param string $id
     * @param int $value
     * @return array
     */
    private function domainEditAction($id, $value)
    {
        $status = ['status' => 0];
        $domains = $this->getDomains();
        if (isset($domains[ $id ]) && ($value == 0 || $value == 1)) {
            $file = $this->basePath.$domains[ $id ].'/maintenance.file';
            if ($value == 0) {
                @unlink($file);
                if (file_exists($file)) {
                    $status['msg'] = 'Не удалось удалить файл! Возможно недостаточно прав.<br>'.$file;
                }
                else {
                    $status['status'] = 1;
                }
            }
            else {
                @file_put_contents($file, '');
                if (!file_exists($file)) {
                    $status['msg'] = 'Не удалось создать файл! Возможно недостаточно прав.<br>'.$file;
                }
                else {
                    $status['status'] = 1;
                }
            }
        }
        return $status;
    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
    }
}

Dominator::getInstance()->run();