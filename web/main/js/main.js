$( function() {

    $('.btn-action').click(function () {
        var $this = $(this);
        var id = $this.data('id');
        var value = $this.data('value');
        if (id != '' && (value == 0 || value == 1)) {
            $.ajax({
                type: "POST",
                async: false,
                dataType: "json",
                data: { action : 'domainEdit' , id : id , value : value },
                success: function(response){
                    if (response.status == 1) {
                        if (value == 0) {
                            $this.data('value',1);
                            $this.closest('tr')
                                .find('.label-action')
                                .removeClass('label-danger')
                                .addClass('label-success')
                                .html('Активирован');
                        }
                        else {
                            $this.data('value',0);
                            $this.closest('tr')
                                .find('.label-action')
                                .removeClass('label-success')
                                .addClass('label-danger')
                                .html('Деактивирован');
                        }
                    }
                    else {
                        alert( response.msg );
                        return false;
                    }
                }
            });
        }
    });

});
